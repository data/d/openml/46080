# OpenML dataset: Largest_Companies_in_the_World

https://www.openml.org/d/46080

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset named "largest_companies_by_number_of_employees.csv" provides a comprehensive listing of various companies, ranking them based on their total number of employees. This dataset could be an essential tool for analysts and researchers focusing on labor economics, corporate size, and employment patterns across different regions and sectors. It contains critical data points for each listed company, including its rank based on the number of employees, the official name, the stock market symbol, the exact number of employees, the current stock price in USD, and the country in which the company is headquartered.

Attribute Description:
- Rank: A numeric value indicating the company's position relative to others based on the number of employees. Sample values include 4399, 6350, 1265, etc.
- Name: The official name of the company. Examples include 'NHPC Limited', 'Colliers International', etc.
- Symbol: The stock market symbol under which the company is listed. For instance, 'DKNG', 'SINT', 'SPARC.NS'.
- Employees_count: The total number of individuals employed by the company. Sample numbers are 12060, 2500, 9788, etc.
- Price (USD): The current stock price of the company in United States Dollars. Example values are 4.14, 6.55746, 1.58128, etc.
- Country: The country in which the company's headquarters is located. Examples include 'United States', 'Italy', 'Japan'.

Use Case:
This dataset is particularly useful for market researchers, financial analysts, and sociologists who are interested in understanding employment trends, assessing the scale of operations of different companies, and analyzing the economic impact of these corporations globally. Furthermore, it can aid in investment decision-making processes by providing insights into the company's size and market value.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46080) of an [OpenML dataset](https://www.openml.org/d/46080). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46080/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46080/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46080/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

